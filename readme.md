

### Popis spuštění:

Důležité je si stáhnou správnou Javu 11. Já osobně jsem si stáhl zde : https://jdk.java.net/archive/
Konkrétně verzi 11.0.2. To si potom musíme správně naimportovat do IntelliJ.

Dále je potřeba si stáhnou zdroják z moodle. VPTI_01. (Poprvé asi udělat z moodlu, až to bude fungovat, až potom si to spojit tady s tímto Repem.)

V IntelliJ si projekt naimportuju přes: File -> New -> Project from Existing Sources -> potom si vyberu z eclipse a namapuji si ten projekt (Tady před tím je dobré/vhodné si dát adresář s kódem někam jinam z Downloads, např. do IntelliJ/pda nebo tak něco)

Teď by měl být projekt dobře naimportovaný. Teď je nutné si správně nastavit knihovny a JDK 11. To se děje v File -> Project Structure.

Knihovny se dělají v Libraries -> přes tlačítko plus naimportuji všechny fily ze složky lib/ z VPTI_01

JDK se mění malinko složitěji. V těch Project Structure -> SDK přes plus si namapuji tu staženou JDK viz. bod 1.
Pokud to nehází nějaký bordel tak dám apply a mělo by to být funkční. 

Nutné podotknout, že sevyskočí hned okno s tanky, ale je potřeba si v src/tanks/RobocodeRunner na řádku 73 změnit Visibility na true
